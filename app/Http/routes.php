<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', [
    'uses' => 'AgeController@showIndex'
]);

$app->post('/age-in-hours', [
    'uses' => 'AgeController@showAge'
]);

$app->get('/view-all', [
    'uses' => 'AgeController@showSubmissions'
]);

//$app->get('configure/socket/{socketID}', [
//    'uses' => 'ConfigController@showIndex'
//]);