<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;

class AgeController extends BaseController
{
    public function showIndex()
    {
        return view('index', []);
    }

    public function showAge(Request $request)
    {
        try
        {
            $from = new \DateTime($request->input('age'));
            $to   = new \DateTime('today');
            $diff = $from->diff($to);

            if($diff)
            {
                $this->saveSubmission($request, $diff);
            }

            return json_encode(['years' => $diff->y, 'days' => $diff->d, 'hours' => $diff->h]);
        }
        catch(\Exception $e)
        {
            $r = new Response();
            return $r->status(500);
        }
    }

    public function showSubmissions()
    {
        $subs = $this->getPastSubmissions();
        return view('submissions', ['subs' => $subs]);
    }

    protected function saveSubmission($request, $diff)
    {
        $sub = new User;
        $sub->name = $request->input('name');
        $sub->dob = $request->input('age');
        $sub->years = $diff->y;
        $sub->days = $diff->d;
        $sub->hours = $diff->h;
        $sub->save();
    }

    protected function getPastSubmissions()
    {
        $allSubs = User::paginate(10);
        return $allSubs;
    }


}
