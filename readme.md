# Age Calculator

##Description

Shows your age in years, days, hours and stores past submissions.

## Install

Checkout repository.
run 'composer up'

Test website.

If any Database errors occur please do the following:

    Delete the file 'database\database.sqlite'
    Run the command 'touch database\database.sqlite'
    Then run the command 'php artisan migrate'

This should fix any issues created by the DB.


## Framework

Built using Lumen the micro framework from Laravel

## License

The Lumen framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)