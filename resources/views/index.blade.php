<html>
    <head>

        <meta charset="utf-8">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">

        <link href="css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/fontawesome.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/jquery-ui.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/theme.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/age.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/jquery.datetimepicker.min.css" media="all" rel="stylesheet" type="text/css" />


        <title>Age Revealer 9001</title>
    </head>
    <body>
        <div class="container container-table">
            <div class="row vertical-center-row">
                <div class="col-xs-offset-4 col-xs-4">

                    <nav class="navbar navbar-inverse">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                    <ul class="nav navbar-nav">
                                        <li><a href="/">Show age</a></li>
                                        <li><a href="/view-all">Past submissions</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </nav>

                    <form action="#" name="dateForm">
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <label class="sr-only" for="name">Name: </label>
                                <input type="text" class="form-control" id="name" placeholder="Enter your Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="ageSelect">Age: </label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="ageSelect" placeholder="Enter your Age">
                                <div class="input-group-addon" id="ageDisplay">00</div>
                            </div>
                        </div>
                        <button type="submit" id="showHours" class="btn btn-primary">Show Me!!!!!</button>
                    </form>

                    <div class="col-xs-12 col-sm-4">
                        Years: <span id="years"></span>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        Days: <span id="days"></span>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        Hours: <span id="hours"></span>
                    </div>

                </div>
            </div>
        </div>

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.datetimepicker.full.min.js"></script>

        <div id="errorModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Error!!</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            Something went very wrong! try again?
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>



        <script>
            $(function() {

                function ageFromDate(inputAge)
                {
                    var today = new Date();
                    var fromDate = inputAge.split("-");
                    var birthDate = new Date(fromDate[2].substring(0, 4), fromDate[1] - 1, fromDate[0]);

                    //work out the correct age
                    var age = today.getFullYear() - birthDate.getFullYear();
                    var month = today.getMonth() - birthDate.getMonth();
                    if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate()))
                    {
                        age--;
                    }

                    return age;
                }

                $("#ageSelect").datetimepicker({
                    format:'d-m-Y H:m'
                });

                $("#ageSelect").change(function()
                {
                    var age = ageFromDate($("#ageSelect").val());

                    //shows the age in the bootstrap group addon
                    $('#ageDisplay').html(age);
                });

                $('#showHours').click(function(){
                    $.ajax({
                        url: "/age-in-hours",
                        method: 'post',
                        dataType: "json",
                        data: { 'name': $("#name").val(),
                                'age': $("#ageSelect").val()}
                    }).success(function(data) {
                        //console.log(data);
                        $("#years").html(data.years);
                        $("#days").html(data.days);
                        $("#hours").html(data.hours);
                    }).fail(function() {
                        $('#errorModal').modal('show');
                    });
                });
            });
        </script>

    </body>
</html>