<html>
    <head>

        <meta charset="utf-8">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">

        <link href="css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/fontawesome.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/jquery-ui.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/theme.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/age.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/jquery.datetimepicker.min.css" media="all" rel="stylesheet" type="text/css" />


        <title>Age Revealer 9001</title>
    </head>
    <body>
        <div class="container container-table">
            <div class="row vertical-center-row">
                <div class="col-xs-offset-4 col-xs-4">
                    <div class="container col-xs-12">

                        <nav class="navbar navbar-inverse">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                        <ul class="nav navbar-nav">
                                            <li><a href="/">Show age</a></li>
                                            <li><a href="/view-all">Past submissions</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </nav>


                        <table class="table table-striped">
                            <tr>
                                <th>Name</th>
                                <th>Years</th>
                                <th>Days</th>
                                <th>Hours</th>
                            </tr>
                        <?php foreach ($subs as $sub){ ?>
                            <tr>
                                <td><?php echo $sub->name ?></td>
                                <td><?php echo $sub->years ?></td>
                                <td><?php echo $sub->days ?></td>
                                <td><?php echo $sub->hours ?></td>
                            </tr>
                        <?php } ?>
                            <tr>
                                <td colspan="4" class="text-center">
                                <?php echo $subs->links(); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.datetimepicker.full.min.js"></script>



        <div id="errorModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Error!!</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            Something went very wrong! try again?
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>



        <script>
            $(function() {
                function ageFromDate(inputAge){
                    var today = new Date();
                    var fromDate = inputAge.split("-");
                    var birthDate = new Date(fromDate[2].substring(0, 4), fromDate[1] - 1, fromDate[0]);

                    //work out the correct age
                    var age = today.getFullYear() - birthDate.getFullYear();
                    var month = today.getMonth() - birthDate.getMonth();
                    if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate()))
                    {
                        age--;
                    }

                    return age;
                }

                $("#ageSelect").datetimepicker({
                    format:'d-m-Y H:m'
                });

                $("#ageSelect").change(function()
                {
                    var age = ageFromDate($("#ageSelect").val());
                    //shows the age in the bootstrap group addon
                    $('#ageDisplay').html(age);
                });

                $('#showHours').click(function(){
                    $.ajax({
                        url: "/age-in-hours",
                        method: 'post',
                        dataType: "json",
                        data: { 'name': $("#name").val(),
                                'age': $("#ageSelect").val()}
                    }).success(function(data) {
                        //console.log(data);
                        $("#years").html(data.years);
                        $("#days").html(data.days);
                        $("#hours").html(data.hours);
                    }).fail(function() {
                        $('#errorModal').modal('show');
                    });
                });
            });
        </script>

    </body>
</html>